var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mezuOsoa = '';
const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const TextCommand = Telegram.TextCommand
const Telegram_KEY = process.env.DB_PATH? path.join(process.env.DB_PATH, 'telegramKey.txt') : 'telegramKey.txt';
const tg = new Telegram.Telegram(Telegram_KEY);
const request = require('request');
const parseString = require('xml2js').parseString;
const stripPrefix = require('xml2js').processors.stripPrefix;
const fs = require('fs');
const path = require('path');
////twit
const twit = require('twit');
const config = require('./twitconfig.js');
const Twitter = new twit(config);
//date
var moment = require('moment');
moment.defaultFormat = "YYYY-MM-DD HH:mm +02:00";
moment.defaultFormatUtc="Europe/Madrid";

var registered = [];
var herrialdeLista = [];
var herriArray=[];
var lastHerria = '';
var str = '';

//////////////irakurri piztean erregistratuak

const HARPIDETUAK_FILE = process.env.DB_PATH? path.join(process.env.DB_PATH, 'harpidetuak.txt') : 'harpidetuak.txt';
const AZKENA_FILE = process.env.DB_PATH? path.join(process.env.DB_PATH, 'azkena.txt') : 'azkena.txt';
const MEZUA_FILE = process.env.DB_PATH? path.join(process.env.DB_PATH, 'mezua.txt') : 'mezua.txt';

var array = fs.readFileSync(HARPIDETUAK_FILE).toString().split("\n");

for(var i in array) {
	console.log('hasieran irakurri '+array[i]);
	if(array[i]!=''){
		registered.push(array[i]);
		console.log('hasieran register '+registered[i]);
	}
}
///////irakurri piztean azken titulua
var lastHerria = fs.readFileSync(AZKENA_FILE).toString();

//ibilbidea

const IBILBIDEA_FILE = process.env.DB_PATH? path.join(process.env.DB_PATH, 'ibilbidea.txt') : 'ibilbidea.txt';

var array = fs.readFileSync(IBILBIDEA_FILE).toString().split("\n");
var herriak = [];

for(var i in array) {
	//console.log('hasieran irakurri '+array[i]);
	if(array[i]!=''){
		herriak.push(array[i]);
		console.log('hasieran register herriak '+herriak[i]);
	}
}

//////
class mezuaController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    mezuaHandler($) {
      if(!fs.existsSync(MEZUA_FILE)){
				fs.createWriteStream(MEZUA_FILE);
			}
			var mezua = fs.readFileSync(MEZUA_FILE).toString();
			if(mezua!=null||mezua!=""){
				$.sendMessage(mezua);
			}else{
        $.sendMessage('Gasteiza iristean jakingo da mezua.');
			}
    }

    get routes() {
        return {
            'mezuaCommand': 'mezuaHandler'
        }
    }
}

class rssController extends TelegramBaseController {

    /**
     * @param {Scope} $
     */
    rssHandler($) {
      //$.sendMessage('rss')
      request('http://www.korrika.eus/?format=feed&type=rss', (err, res, body) => {
        parseString(body, (err, result) => {
          //var item=result.rss.channel[0].item.length;
          for (var i in result.rss.channel[0].item) {
            var j=+i+ 1;
            $.sendMessage(j +'.- '+ result.rss.channel[0].item[i].title[0]+' : ' + result.rss.channel[0].item[i].link[0]);
          }
        })
      })
    }
    get routes() {
      return {
        'rssCommand': 'rssHandler'
      }
    }
}

class nonController extends TelegramBaseController {

    /**
     * @param {Scope} $
     */
    nonHandler($) {

			  console.log('moment'+moment().format('MMMM Do YYYY, h:mm+02:00'));
		  var send=false;
			var AurrekoherriaAfter=false;
			var AurrekoHerria='';
			var AurrekoProbintzia='';
			for(var i in herriak) {
		    var array = herriak[i].toString().split(",");
		    if(array['2']!='' && !send){
		  		if(moment().isBefore(array['2']+'+02:00') && AurrekoherriaAfter){
						$.sendMessage(AurrekoHerria+' herrian doa #korrika21, '+AurrekoProbintzia+'n.\nHurrengo herria '+array['0']+', '+array['1']+', da.');
						$.sendMessage();
						send=true;
						AurrekoherriaAfter=false;
					}else{
						AurrekoherriaAfter=true;
						AurrekoHerria=array['0'];
						AurrekoProbintzia=array['1'];
					}
				}
			}

    }

    get routes() {
        return {
            'nonCommand': 'nonHandler'
        }
    }
}


class noizController extends TelegramBaseController {

    noizHandler($) {
        $.sendMessage('Laster egongo da eskuragarri komando hau. Barkatu eragozpenak.')
}

get routes() {
    return {
        'noizCommand': 'noizHandler'
    }
}
}

///////////////////////////////////
class RegisterController extends TelegramBaseController {
  /**
   * @param {Scope} $
   */
  registerHandler($) {
    let chatId = $._message._chat._id;
    let type = $._message._chat._type;
    console.log(registered);
    if (registered.indexOf(chatId) > -1) {
      if (type === 'group') {
        return $.sendMessage('Taldea korrika ibilbidera erregistratu da.');
      } else {
        return $.sendMessage('Korrika ibilbidera erregistratuta zaude iada.');
      }
    }


//Irakurri

if(!fs.existsSync(HARPIDETUAK_FILE)){
	fs.createWriteStream(HARPIDETUAK_FILE);
}
registered = [];
var array = fs.readFileSync(HARPIDETUAK_FILE).toString().split("\n");

for(var i in array) {
	console.log('harpidetza irakurri'+array[i].toString());
	if(array[i].toString()!=''){
		registered.push(array[i].toString());
		console.log('harpidetza register'+registered[i]);
	}
}

if(registered.indexOf(chatId.toString()) === -1){
	registered.push(chatId);
}

//Idatzi
fs.unlinkSync(HARPIDETUAK_FILE);
for(var i in registered) {
	console.log('harpidetza idatzi'+registered[i]);
	fs.appendFileSync(HARPIDETUAK_FILE, registered[i]+"\n");
}

//registered.push(chatId);
	return $.sendMessage('erregistroa kentzeko: /harpidetza_utzi.');
  }

  get routes() {
    return {
      'registerCommand': 'registerHandler'
    }
  }
}

class UnregisterController extends TelegramBaseController {
  /**
   * @param {Scope} $
   */
  unregisterHandler($) {
    let chatId = $._message._chat._id;
 //   if (registered.indexOf(chatId) > -1) {
//      registered.splice(registered.indexOf(chatId), 1);
//      console.log(registered);

//Irakurri
if(!fs.existsSync(HARPIDETUAK_FILE)){
	fs.createWriteStream(HARPIDETUAK_FILE);
}
registered = [];
var array = fs.readFileSync(HARPIDETUAK_FILE).toString().split("\n");

for(var i in array) {
	console.log('un irakurri'+array[i]);
	if(array[i]!=''){
		registered.push(array[i]);
		console.log('register'+registered[i]);
	}
}

if(registered.indexOf(chatId.toString())!= -1){
console.log('un splice'+chatId.toString());
registered.splice(registered.indexOf(chatId.toString()), 1);
}

//Idatzi
fs.unlinkSync(HARPIDETUAK_FILE);
for(var i in registered) {
	console.log('idatzi'+registered[i]);
	if(registered[i]!=''&&registered[i]!='\n'){
		fs.appendFileSync(HARPIDETUAK_FILE, registered[i]+"\n");
	}
}

      return $.sendMessage('Ibilbideko berriak jasotzeari utziko diozu.');
  //  }
  }

  get routes() {
    return {
      'unregisterCommand': 'unregisterHandler'
    }
  }
}

/////////////////////routers
tg.router.when(
  new TextCommand('/harpidetu', 'registerCommand'),
  new RegisterController()
)
tg.router.when(
  new TextCommand('/harpidetza_utzi', 'unregisterCommand'),
  new UnregisterController()
)
tg.router.when(
  new TextCommand('/berria', 'rssCommand'),
  new rssController()
)
tg.router.when(
  new TextCommand('/lekukoarenmezua', 'mezuaCommand'),
  new mezuaController()
)
tg.router.when(
  new TextCommand('/nondago', 'nonCommand'),
  new nonController()
)
tg.router.when(
  new TextCommand('/noiznireherrian', 'noizCommand'),
  new noizController()
)
/////////////

setInterval(() => {
  console.log('moment'+moment().format('MMMM Do YYYY, h:mm+02:00'));

	var lastHerria = fs.readFileSync(AZKENA_FILE).toString();
  var send=false;
	var AurrekoHerria='';
	var AurrekoProbintzia='';
  for(var i in herriak) {
    var array = herriak[i].toString().split(",");
		if(!send){
    	if(array['2']!=''){
  			if(moment().isAfter(array['2']+'+02:00')){
					console.log('txio ez: '+array['0']+moment().isAfter(array['2']+'+02:00'));
					AurrekoHerria=array['0'];
					AurrekoProbintzia=array['1'];
				}else{
					console.log('Bota: '+array['0']+moment().isAfter(array['2']+'+02:00'));
					UnekoHerria=array['0'];
					if(AurrekoHerria!=lastHerria){
						fs.unlinkSync(AZKENA_FILE);
						console.log('azkena gorde:'+AurrekoHerria );
						fs.appendFileSync(AZKENA_FILE, AurrekoHerria );
						lastHerria=AurrekoHerria;
						send=true;
						// //txio
						Twitter.post('statuses/update', { status:'Hemen dago orain: ' + AurrekoHerria+ ' #korrika21' + ' #'+AurrekoProbintzia }, function(err, data, response) {
							if(err){
								console.log("Error in tweeting azken posta");
							}
							else{
						    send=true;
								console.log("Azken posta shown successfully");
							}
						});

						for (var i in registered) {
							tg.onMaster(() => {
								console.log('setInterval besteetan bota: '+registered);
								tg.api.sendMessage(registered[i],'Hemen dago orain: ' + AurrekoHerria + ' #korrika21' + ' #'+AurrekoProbintzia );
							})
						}

					}
				}
			}//end if denbora null
		}//end if send
  }//end for
},1000 * 60 * 5);
