# Kodea martxan jartzeko

## Falta diren fitxategiak:

harpidetuak.txt: hau hasieran hutsa izan daiteke

twitconfig.js: hona hemen adibidea https://gitlab.com/utolosa002/korrikabot/blob/master/twitconfig-example.js

ibilbidea.txt: ibilbidea formatu honetan sartu behar da: https://gitlab.com/utolosa002/korrikabot/blob/master/ibilbidea-example.txt

index.js fitxategian Telegram_KEY zurea sartu.

## Exekutatu

`$ npm init`

`$ node index.js`

# Martxan
## https://twitter.com/KorrikaB

## https://t.me/KorrikaBot